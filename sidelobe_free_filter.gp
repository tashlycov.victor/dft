set terminal pngcairo size 2700,1500 enhanced font 'arial, 30' lw 2
set output sprintf("sidelobe_free_filter.png")

set multiplot layout 3,2\
    margins 0.1,0.95,0.15,0.95\
    spacing 0.05,0.05

set title 
set zeroaxis
set xtics 5
set grid

file1 = 'conv.dat'
file2 = 'hc.dat'
file3 = 'conv_dft.dat'
file4 = 'hc_dft.dat'
file5 = 'multi_dft.dat'
file6 = 'multi_dft_rev.dat'


# set xrange[0:20]
# set yrange[-1.1:1.1]
plot file1 using 1:2 with linespoints title 'Real' lw 2
unset yrange
unset xrange
set xtics 5
# set xrange[0:20]
# plot file3 using 1:2 with linespoints title 'Real'
plot file3 using 1:2 with linespoints title 'Real' lw 2,\
     file3 using 1:3 with linespoints title 'Image' lw 2


# set xrange[0:20]
# set yrange[-1.1:1.1]
plot file2 using 1:2 with linespoints title 'Real' lw 2
# unset yrange
unset xrange
# set yrange[-40:40]

# set xrange[0:20]
# set logscale y
plot file4 using 1:2 with linespoints title 'Real' lw 2,\
    file4 using 1:3 with linespoints title 'Image' lw 2

unset yrange
unset logscale y
unset xrange
# set xtics 100
# set xrange[0:20]
plot file5 using 1:2 with linespoints title 'Real' lw 2,\
    file5 using 1:3 with linespoints title 'Image' lw 2
# set yrange[-1.1:1.1]
plot file6 using 1:2 with linespoints title 'Real' lw 2





