#
CXX = g++
CXXFLAGS = -Wextra -g3 -O3 -std=c++14 -fopenmp -lpthread -I/gnuplot-iostream
#
main: main.o math_funcs.o fftw_use.o
	$(CXX) $(CXXFLAGS) -o main main.o math_funcs.o fftw_use.o -lfftw3
weight: weight_func.o math_funcs.o fftw_use.o
	$(CXX) $(CXXFLAGS) -o weight_func weight_func.o math_funcs.o fftw_use.o -lfftw3
	./weight_func
dft: dft_class_usage.o math_funcs.o fftw_use.o dft.o
	$(CXX) $(CXXFLAGS) -o dft_class_usage dft_class_usage.o math_funcs.o fftw_use.o dft.o -lfftw3
	./dft_class_usage
clean:
	rm *.o
