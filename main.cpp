#include "fftw_use.h"
#include "math_funcs.h"

int main() {
    const int N = 39; // Размер массива данных(in, out)

    fftw_complex *in, *out;
    in = (fftw_complex *)fftw_malloc(sizeof(fftw_complex) * m);
    out = (fftw_complex *)fftw_malloc(sizeof(fftw_complex) * m);

    vector<double> H(m);
    vector<double> Q(m);
    vector<double> L(m);
    vector<double> l(m);

    void(*funcs[])(const int, fftw_complex*) =
    {
        rect_func_reversed, barker_code13
    };

    // Использование FFTW
    funcs[1](N, in);
    print_to_file("basic_H.dat", N, in);
    using_fftw(N, in, out, H, true);
    funcs[0](N, in);
    print_to_file("basic_Q.dat", N, in);
    using_fftw(N, in, out, Q, true);
    for(int i = 0; i < m; i++)
        L[i] = Q[i] / H[i];

    vector2fftwcmpl(N, L, in);
    using_fftw(N, in, out, l, false);
    std::ofstream file_out("fourier_trans.dat");
    for (int i = 0; i < m / 2; i++) {
        file_out << i - m / 2 << "\t";
        file_out << H[i + m / 2 - 1] << "\t";
        file_out << Q[i + m / 2 - 1] << "\t";
        file_out << L[i + m / 2 - 1] << "\t";
        file_out << l[i] << "\n"; 
    }
    for (int i = 0; i < m / 2; i++) {
        file_out << i << "\t";
        file_out << H[i] << "\t";
        file_out << Q[i] << "\t";
        file_out << L[i] << "\t";
        file_out << l[i + m / 2 - 1] << "\n"; 
    }
    
    file_out.close();

    return 0;
}
