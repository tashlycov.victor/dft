#include "fftw_use.h"
#include "math_funcs.h"

using namespace std;

int main(){
    const int bits = 13;
    const int units = 3;
    const int N = bits * units; // Размер массива данных(in, out)

    fftw_complex *in, *out;
    in = (fftw_complex *)fftw_malloc(sizeof(fftw_complex) * N);
    out = (fftw_complex *)fftw_malloc(sizeof(fftw_complex) * N);

    vector<double> p(N);
    vector<double> Hc(N);
    vector<double> conv(N);
    vector<double> conv2(N);
    vector<double> conv3(2 * N + 1);
    vector<double> dft_res(N);
    vector<double> dft_rev_res(N);

    for(int i = 0; i < N; i++)
        p[i] = p_func(i, units);
    print_to_file_vec("p.dat", N, p);

    barker_code13(N, in);

    for(int i = 0; i < N; i++)
        Hc[i] = h(in, bits, i, units);
    print_to_file_vec("hc.dat", N, Hc);

    for(int i = 0; i < N; i++)
        conv[i] = convolution(i, units, true);
    print_to_file_vec("conv.dat", N, conv);

    for(int i = 0; i < N; i++)
        conv2[i] = convolution(i, units, false);
    print_to_file_vec("conv_rev.dat", N, conv2);

    for(int i = 0; i < 2 * N; i++){
        conv3[i] = 0;
        for(int j = 0; j < N; j++)
            if(i - j >= 0 && i - j <= N)
                conv3[i] += conv[j] * conv2[i - j];
    }
    
    print_to_file_vec("conv_multi.dat", conv3.size(), conv3);

    for(int i = 0; i < N; i++)
        dft_res[i] = dft(i, N);
    print_to_file_vec("dft_res.dat", N, dft_res);

    for(int i = 0; i < N; i++)
        dft_rev_res[i] = dft_rev(i, N);
    print_to_file_vec("dft_rev_res.dat", N, dft_rev_res);

}