set terminal pngcairo size 2700,1500 enhanced font 'arial, 30' lw 2
set output sprintf("fourier_transform_yrange_%s.png", ARG1)

set multiplot layout 3,2\
    margins 0.1,0.95,0.15,0.95\
    spacing 0.1,0.12

N = ARG1 + 0
set title 
set zeroaxis
# set xlabel 'Index'
# set ylabel 'Value'
set xtics 100
set grid

file = 'fourier_trans.dat'
file2 = 'basic_H.dat'
file3 = 'basic_Q.dat'

set xrange[0:40]
plot file2 using 1:2 with linespoints title 'Real'
unset xrange
plot file using 1:2 with linespoints title 'Real'

set xrange[0:40]
set yrange[-0.1:1.1]
plot file3 using 1:2 with linespoints title 'Real'
unset yrange
unset xrange
plot file using 1:3 with linespoints title 'Real'

plot file using 1:4 with linespoints title 'Real'
set xrange[-450:450]
plot file using 1:5 with linespoints title 'Real'


