set terminal pngcairo size 2700,1500 enhanced font 'arial, 30' lw 2
set output sprintf("dft.png")

set multiplot layout 3,2\
    margins 0.1,0.95,0.15,0.95\
    spacing 0.1,0.12

set title 
set zeroaxis
# set xlabel 'Index'
# set ylabel 'Value'
set xtics 5
set grid

file1 = 'p.dat'
file2 = 'hc.dat'
file3 = 'p_dft.dat'
file4 = 'hc_dft.dat'
file5 = 'multi_dft.dat'
file6 = 'multi_dft_rev.dat'


set xrange[0:40]
set yrange[-1.1:1.1]
plot file1 using 1:2 with linespoints title 'Real'
unset yrange
unset xrange
plot file3 using 1:2 with linespoints title 'Real'

set xrange[0:40]
set yrange[-1.1:1.1]
plot file2 using 1:2 with linespoints title 'Real'
unset yrange
unset xrange
plot file4 using 1:2 with linespoints title 'Real'

unset yrange
unset xrange
plot file5 using 1:2 with linespoints title 'Real'
# set yrange[-1.1:1.1]
# set logscale y
plot file6 using 1:2 with linespoints title 'Real'





