#include "fftw_use.h"

const int m = 1024;

void using_fftw(const int N, fftw_complex* in, fftw_complex* out, std::vector<complex<double> > &H, bool is_forward){
    fftw_plan p;
    if(is_forward){
        p = fftw_plan_dft_1d(N, in, out, FFTW_FORWARD, FFTW_ESTIMATE);
        fftw_execute(p);
        for(int i = 0; i < N; i++)
            H[i] = complex<double> (out[i][0], out[i][1]);
    }
    else{
        p = fftw_plan_dft_1d(N, in, out, FFTW_BACKWARD, FFTW_ESTIMATE);
        fftw_execute(p);
        for(int i = 0; i < N; i++)
            H[i] = complex<double> (out[i][0], out[i][1]);
    }
    fftw_destroy_plan(p);
}


void print_to_file(const char *filename, const int N, fftw_complex* fftw_stream, bool is_mirror){
    std::ofstream file_out(filename);
    if(is_mirror){
        for (int i = 0; i < N / 2; i++) 
            file_out << i - N / 2 << " " << fftw_stream[i + N / 2 - 1][0] * fftw_stream[i + N / 2 - 1][0] + fftw_stream[i + N / 2 - 1][1] * fftw_stream[i + N / 2 - 1][1] << "\n";
        
        for (int i = 0; i < N / 2; i++) 
            file_out << i << " " << fftw_stream[i][0] * fftw_stream[i][0] + fftw_stream[i][1] * fftw_stream[i][1] << "\n";
        
    }
    else
        for (int i = 0; i < N; i++) 
            file_out << i << " " << fftw_stream[i][0] << "\n";
            // file_out << i << " " << fftw_stream[i][0] * fftw_stream[i][0] + fftw_stream[i][1] * fftw_stream[i][1] << "\n";
        
    file_out.close();
}

void print_to_file_vec(const char *filename, const int N, std::vector<complex<double> > vec, bool is_mirror){
    std::ofstream file_out(filename);
    for (int i = 0; i < N; i++)
        file_out << i << " " << real(vec[i]) << " "<<imag(vec[i]) << "\n";
    
    file_out.close();
}
