#include <iostream>
#include "dft.h"
#include "math_funcs.h"
#include "fftw_use.h"

using namespace std;

int main(){
    const int bits = 5;
    const int units = 3;
    const int N = 2*bits*units;

    // fftw_complex *in, *out, *p_dft, *Hc_dft;
    fftw_complex *in, *out, *conv_dft, *Hc_dft;
    // p_dft = (fftw_complex *)fftw_malloc(sizeof(fftw_complex) * N);
    conv_dft = (fftw_complex *)fftw_malloc(sizeof(fftw_complex) * N);
    Hc_dft = (fftw_complex *)fftw_malloc(sizeof(fftw_complex) * N);
    in = (fftw_complex *)fftw_malloc(sizeof(fftw_complex) * N);
    out = (fftw_complex *)fftw_malloc(sizeof(fftw_complex) * N);

    DFT dft_use(N);
    // vector<double> p(N);
    vector<complex<double> > Hc(N);

    // Сравнение работы DFT с FFTW
    // for(int i = 0; i < N; i++)
    //     p[i] = p_func(i, units);
    // print_to_file_vec("p.dat", N, p);
    
    // barker_code13(bits, in);

    // for(int i = 0; i < N; i++)
    //     Hc[i] = h(in, bits, i, units);
    // print_to_file_vec("hc.dat", N, Hc);

    // vector2fftwcmpl(N, p, in);
    // dft_use.find_transform(in, p_dft, true);
    // print_to_file("p_dft.dat", N, p_dft, true);

    // vector2fftwcmpl(N, Hc, in);
    // dft_use.find_transform(in, Hc_dft, true);
    // print_to_file("hc_dft.dat", N, Hc_dft, true);

    // for(int i = 0; i < N; i++){
    //     in[i][0] = p_dft[i][0] * Hc_dft[i][0];
    //     in[i][1] = p_dft[i][1] * Hc_dft[i][1];
    // }

    // print_to_file("multi_dft.dat", N, in, true);
    // dft_use.find_transform(in, out, false);
    // print_to_file("multi_dft_rev.dat", N, out, true);

    // Отрисовка sidelobe-free filter
    barker_code5(bits, in);

    for(int i = 0; i < N; i++)
        Hc[i] = i < N ? complex<double> (h(in, bits, i, units), 0.0) : complex<double> (0.0, 0.0);
    
    print_to_file_vec("hc.dat", N, Hc);
    vector2fftwcmpl(N, Hc, in);
    using_fftw(N, in, Hc_dft, Hc, true);
    for(int i = 0; i < N; i++){
        complex<double> x(Hc_dft[i][0], Hc_dft[i][1]);
        Hc_dft[i][0] = real(1.0 / x);
        Hc_dft[i][1] = imag(1.0 / x);
    }
    // print_to_file("hc_dft.dat", N, Hc_dft, true);
    ofstream fout_hc("hc_dft.dat");
    ofstream fout_conv("conv_dft.dat");
    ofstream fout_multi("multi_dft.dat");
    ofstream fout_multi_rev("multi_dft_rev.dat");
    const int k = N % 2 == 0 ? N / 2 : N / 2 - 1; // Если n - нечётное
    for (int i = 0; i < k; i++) 
        fout_hc << i - k << " " << Hc_dft[i + k][0] << " " << Hc_dft[i + k][1] << "\n";
    for (int i = 0; i < k ; i++) 
        fout_hc << i << " " << Hc_dft[i][0] << " " << Hc_dft[i][1] << "\n";
    
    for(int i = 0; i < N; i++)
        in[i][0] = i < N ? convolution(i, units, true) : 0;
    print_to_file("conv.dat", N, in);
    // dft_use.find_transform(in, conv_dft, true);
    using_fftw(N, in, conv_dft, Hc, true);
    // print_to_file("conv_dft.dat", N, conv_dft, true);
    for (int i = 0; i < k; i++) 
        fout_conv << i - k << " " << conv_dft[i + k][0] << " " << conv_dft[i + k][1] << "\n";
    for (int i = 0; i < k; i++) 
        fout_conv << i << " " << conv_dft[i][0] << " " << conv_dft[i][1] << "\n";

    for(int i = 0; i < N; i++){
        in[i][0] = conv_dft[i][0] * Hc_dft[i][0] - conv_dft[i][1] * Hc_dft[i][1];
        in[i][1] = conv_dft[i][1] * Hc_dft[i][0] + conv_dft[i][0] * Hc_dft[i][1];
    }

    // print_to_file("multi_dft.dat", N, in, true);
    // for (int i = 0; i < k; i++) 
    //     fout_multi << i - k << " " << in[i + k][0] << " " << in[i + k][1] << "\n";
    for (int i = 0; i < N; i++) 
        fout_multi << i << " " << in[i][0] << " " << in[i][1] << "\n";
    
    using_fftw(N, in, out, Hc, false);
    for(int i = 0; i < N; i++)
        fout_multi_rev << i << " " << out[i][0] << " " << out[i][1] << endl;

    fftw_free(in);
    fftw_free(out);
    // fftw_free(p_dft);
    fftw_free(conv_dft);
    fftw_free(Hc_dft);

}