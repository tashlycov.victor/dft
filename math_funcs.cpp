#include "math_funcs.h"

void delta_func(const int N, fftw_complex* in){
    for (int i = 0; i < N; i++) {
        in[i][0] = (i == 0) ? 1.0 : 0.0; 
        in[i][1] = 0.0;
    }
}

void rect_func_reversed(const int N, fftw_complex *in){
    const int K = 3;// Размер прямоугольного импульса
    for (int i = 0; i < N; i++) {
        in[i][0] = (i > N - K - 1) ? 1.0 : 0.0; 
        in[i][1] = 0.0;
    }
}

void rect_func(const int N, fftw_complex *in){
    const int K = 3;// Размер прямоугольного импульса
    for (int i = 0; i < N; i++) {
        in[i][0] = (i < K) ? 1.0 : 0.0; 
        in[i][1] = 0.0;
    }
}

void cos_func(const int N, fftw_complex *in){
    const double frequency = 100.0;// Частота косинуса
    for (int i = 0; i < N; ++i) {
        in[i][0] = cos(2 * M_PI * frequency * i / N);
        in[i][1] = 0.0;
    }
}

void gauss_func(const int N, fftw_complex *in){
    const double sigma = 50.0; // Коэффициент распределения
    for (int i = 0; i < N; ++i) {
        double x = i - N / 2.0;
        in[i][0] = exp(-0.5 * (x / sigma) * (x / sigma));
        in[i][1] = 0.0;
    }
}

int p_func(const int n, const int units){
    int sum = 0;
    for(int i = 0; i < units; i++)
        sum += impulse_func(i - n);
    
    return sum;
}

vector<double> q_func(vector<double> p){
    vector<double> result;
    for (int i = 0; i < p.size(); ++i)
        result.push_back(p[p.size() - 1 - i]);

    return result;
}

int h(fftw_complex *in, const int bits, const int n, const int units){
    int sum = 0;
    for (int i = 0; i < bits; i++) 
        sum += in[i][0] * impulse_func(n - i * units);
    
    return sum;
}

// int hd(vector<double> vec, const int bits, const int n, const int units){
//     int sum = 0;
//     for (int i = 0; i < bits; i++) 
//         sum += vec[i] * impulse_func(n - i * units);
    
//     return sum;
// }

void vector2fftwcmpl(const int N, vector<complex<double> > &H, fftw_complex* in){
    for(int i = 0; i < N; i++){
        in[i][0] = real(H[i]);
        in[i][1] = imag(H[i]);
    }
}

void fftwcmpl2vector(const int N, vector<complex<double> > &H, fftw_complex* in){
    for(int i = 0; i < N; i++)
        H[i] = complex<double> (in[i][0], in[i][1]);
}

void reverse_fftwcmpl(const int N, fftw_complex* in){
    fftw_complex* in_temp;
    in_temp = (fftw_complex *)fftw_malloc(sizeof(fftw_complex) * m);
    for(int i = 0; i < N; i++)
        in_temp[i][0] = in[i][0];

    for(int i = 0, j = N - 1; i < N; i++, j--){
        in[i][0] = in_temp[j][0];
        in[i][1] = 0;
    }
}

void reverse_vector(const int N, vector<double> &in){
    vector<double> in_temp;
    in_temp = in;

    for(int i = 0, j = N - 1; i < N; i++, j--)
        in[i] = in_temp[j];
    
}

int convolution(const int n, const int units, bool is_forward){
    int sum = 0; 
    fftw_complex *in;
    in = (fftw_complex *)fftw_malloc(sizeof(fftw_complex) * 13);
    barker_code5(5, in);
    if(is_forward){
        for(int i = 0; i < units; i++)
            sum += p_func(i, 3) * h(in, 13, n - i, 3);
    }
    else{
        reverse_fftwcmpl(13, in);
        for(int i = -units; i < units; i++)
            sum += p_func(i, 3) * h(in, 13, n - i, 3);
    }
    return sum;
}

int impulse_func(const int N){
    return N == 0 ? 1 : 0;
}

double dft(const double w, const int N){
    complex<double> res{0.0, 0.0};
    for(double n = -2 * N; n < 2 * N; n++)
        res += double(convolution(n, 3, true)) * exp(-im * n * w);

    return norm(res);
}

double dft_rev(const double n, const int N){
    complex<double> res{0.0, 0.0};
    for(double w = 0; w < 2 * M_PI; w += M_PI / 200)
        res += dft(w, N) * exp(im * n * w);

    return norm(res) / 2 / M_PI;
}

void barker_code5(const int N, fftw_complex *in){
    const int seq = N / 5; //Кол-во отсчётов
    for (int i = 0; i < N; ++i) {
        in[i][0] = (i / seq == 3) ? -1.0 : 1.0;
        in[i][1] = 0.0;
    }
}

void barker_code7(const int N, fftw_complex *in){
    const int seq = N / 7; //Кол-во отсчётов
    for (int i = 0; i < N; ++i) {
        in[i][0] = (i / seq == 3 || i / seq == 4 || i / seq == 6) ? 0.0 : 1.0;
        in[i][1] = 0.0;
    }
}

void barker_code11(const int N, fftw_complex *in){
    const int seq = N / 11; //Кол-во отсчётов
    for (int i = 0; i < N; ++i) {
        in[i][0] = (i / seq == 0 || i / seq == 1 || i / seq == 2 || i / seq == 6 || i / seq == 9) ? 1.0 : 0.0;
        in[i][1] = 0.0;
    }
}

void barker_code13(const int N, fftw_complex *in){
    const int seq = N / 13; //Кол-во отсчётов
    for (int i = 0; i < N; ++i) {
        in[i][0] = (i / seq == 5 || i / seq == 6 || i / seq == 9 || i / seq == 11) ? -1.0 : 1.0;
        in[i][1] = 0.0;
    }
}