#include <fftw3.h>
#include <iostream>
#include <cmath>
#include <complex>

#ifndef _DFT_H
#define _DFT_H

class DFT {
private:
  fftw_complex *in, *out;
  int N;

public:
  DFT(int N);
  void find_transform(fftw_complex *in, fftw_complex *out, bool is_forward);
};

#endif //_DFT_H