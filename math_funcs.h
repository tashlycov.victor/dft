#include <fftw3.h>
#include <iostream>
#include <cmath>
#include <complex>
#include <vector>

#ifndef _MATH_FUNCS_H
#define _MATH_FUNCS_H

using namespace std;

const int m = 1024;
const complex<double> im{0.0, -1.0};

// Математические функции
void delta_func(const int N, fftw_complex* in);
void rect_func(const int N, fftw_complex* in);
void rect_func_reversed(const int N, fftw_complex *in);
void cos_func(const int N, fftw_complex* in);
void gauss_func(const int N, fftw_complex* in);

int impulse_func(const int N);
int p_func(const int n, const int units);
vector<double> q_func(vector<double> p);
int h(fftw_complex* in, const int bits, const int n, const int units);
int hd(vector<double> vec, const int bits, const int n, const int units);
int convolution(const int n, const int units, bool is_forward);
void reverse_fftwcmpl(const int N, fftw_complex* in);
void reverse_vector(const int N, vector<double> &in);
void vector2fftwcmpl(const int N, vector<complex<double> > &H, fftw_complex* in);
void fftwcmpl2vector(const int N, vector<complex<double> > &H, fftw_complex* in);
double dft(const double w, const int N);
double dft_rev(const double n, const int N);


//Коды Баркера
void barker_code5(const int N, fftw_complex* in);
void barker_code7(const int N, fftw_complex* in);
void barker_code11(const int N, fftw_complex* in);
void barker_code13(const int N, fftw_complex* in);


#endif
