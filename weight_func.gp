set terminal pngcairo size 2700,1500 enhanced font 'arial, 30' lw 2
set output sprintf("weight_func.png")

set multiplot layout 3,1\
    margins 0.1,0.95,0.15,0.95\
    spacing 0.1,0.12

set title 
set zeroaxis
# set xlabel 'Index'
# set ylabel 'Value'
set xtics 5
set grid

file1 = 'conv_rev.dat'
file2 = 'conv.dat'
file3 = 'conv_multi.dat'
# file4 = 'dft_res.dat'
# file5 = 'dft_rev_res.dat'


set xrange[0:40]
set yrange[-1.1:1.1]
plot file1 using 1:2 with linespoints title 'Real'
plot file2 using 1:2 with linespoints title 'Real'
unset yrange
unset xrange
plot file3 using 1:2 with linespoints title 'Real'





