#include <fftw3.h>
#include <iostream>
#include <cmath>
#include <complex>
#include <fstream>
#include <string>
#include <vector>

#ifndef _FFTW_USE_H
#define _FFTW_USE_H

using namespace std;

void using_fftw(const int N, fftw_complex* in, fftw_complex* out, vector<complex<double> > &H, bool is_forward);
void print_to_file(const char* filename, const int N, fftw_complex* out, bool is_mirror = false);
void print_to_file_vec(const char *filename, const int N, vector<complex<double> > vec, bool is_mirror = false);

#endif