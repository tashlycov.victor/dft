#include "dft.h"

DFT::DFT(int N){
    this->N = N;
}

void DFT::find_transform(fftw_complex *in, fftw_complex *out, bool is_forward){
    // Реализация на основе формул: https://en.wikipedia.org/wiki/Discrete_Fourier_transform

    for (int k = 0; k < N; ++k) {
        std::complex<double> sum(0.0, 0.0);
        for (int n = 0; n < N; ++n) {
            double angle = 2 * M_PI * k * n / N;
            sum += std::complex<double>(in[n][0], in[n][1]) * std::polar(1.0, 
                is_forward ? -angle : angle); // Полярная форма коплексного числа - форма в виде длина и угол, 
                // равноценно записи e^(i*angel)
        }
        if (!is_forward) 
            sum /= N; // Нормализация результата, так как результат 
            //получаем умноженный на количество элементов
            
        out[k][0] = sum.real();
        out[k][1] = sum.imag();
    }
}
